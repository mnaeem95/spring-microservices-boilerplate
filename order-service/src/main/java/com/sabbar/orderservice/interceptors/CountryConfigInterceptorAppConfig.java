package com.sabbar.orderservice.interceptors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Component
public class CountryConfigInterceptorAppConfig extends WebMvcConfigurerAdapter {
    @Autowired
    CountryConfigInterceptor countryConfigInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry)  {
        registry.addInterceptor(countryConfigInterceptor);
    }
}
