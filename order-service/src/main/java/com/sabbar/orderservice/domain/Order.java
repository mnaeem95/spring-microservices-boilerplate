package com.sabbar.orderservice.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "ordering")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class Order {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Long id;

  @Embedded
  @AttributeOverrides(value = {
    @AttributeOverride(name = "code", column = @Column(name = "customer_code", nullable = false)),
    @AttributeOverride(name = "name", column = @Column(name = "customer_name", nullable = false)),
  })
  private Customer customer;

  @Embedded
  @AttributeOverrides(value = {
          @AttributeOverride(name = "code", column = @Column(name = "product_code", nullable = false)),
          @AttributeOverride(name = "name", column = @Column(name = "product_name", nullable = false)),
  })
  private Product product;

  @Embedded
  @AttributeOverrides(value = {
    @AttributeOverride(name = "street", column = @Column(name = "shipping_street", nullable = false)),
    @AttributeOverride(name = "city", column = @Column(name = "shipping_city", nullable = false)),
    @AttributeOverride(name = "zip", column = @Column(name = "shipping_zip", nullable = false, length = 6)),
    @AttributeOverride(name = "state", column = @Column(name = "shipping_state", nullable = false))
  })
  private Address shippingAddress;

}
