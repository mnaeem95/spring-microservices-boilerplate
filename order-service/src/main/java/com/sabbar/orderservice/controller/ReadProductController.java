package com.sabbar.orderservice.controller;

import com.sabbar.orderservice.service.products.ProductTO;
import com.sabbar.orderservice.service.products.ReadProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/read-products")
public class ReadProductController {

  @Autowired
  private ReadProductService service;

  @GetMapping(path = "/{code}")
  public ResponseEntity<ProductTO> getProduct(@PathVariable String code) {
    return Optional.ofNullable(service.getProductByCode(code))
      .map(product -> new ResponseEntity<>(product, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping(path = "/feign/{code}")
  public ResponseEntity<ProductTO> getProductByCodeWithFeign(@PathVariable String code) {
    return Optional.ofNullable(service.getProductByCodeWithFeign(code))
            .map(product -> new ResponseEntity<>(product, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping()
  public ResponseEntity<List<ProductTO>> getProducts() {
    return Optional.ofNullable(service.getAllProducts())
      .map(products -> new ResponseEntity<>(products, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping(path = "/feign")
  public ResponseEntity<List<ProductTO>> getProductsWithFeign() {
    return Optional.ofNullable(service.getProductsWithFeign())
      .map(products -> new ResponseEntity<>(products, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }
}
