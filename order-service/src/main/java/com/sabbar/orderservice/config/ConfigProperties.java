package com.sabbar.orderservice.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Validated
@Configuration
@ConfigurationProperties(prefix = "order")
@RefreshScope
public class ConfigProperties {

  @Min(0)
  @Max(1)
  @NotNull
  private Double discount;

  @NotNull
  private String productServiceUrl;

  @NotNull
  private String customerServiceUrl;

  public Double getDiscount() {
    return discount;
  }

  public void setDiscount(Double discount) {
    this.discount = discount;
  }

  public String getProductServiceUrl() {
    return productServiceUrl;
  }

  public void setProductServiceUrl(String productServiceUrl) {
    this.productServiceUrl = productServiceUrl;
  }

  public String getCustomerServiceUrl() {
    return customerServiceUrl;
  }

  public void setCustomerServiceUrl(String customerServiceUrl) {
    this.customerServiceUrl = customerServiceUrl;
  }
}
