package com.sabbar.orderservice.service;

public class OrderCreationException extends RuntimeException {
  public OrderCreationException(String message) {
    super(message);
  }
}
