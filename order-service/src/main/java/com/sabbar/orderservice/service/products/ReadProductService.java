package com.sabbar.orderservice.service.products;

import com.sabbar.orderservice.client.product.HystrixProductFeignClient;
import com.sabbar.orderservice.client.product.ProductClient;
import com.sabbar.orderservice.config.ConfigProperties;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReadProductService {
  @Autowired
  private ConfigProperties properties;

  @Autowired
  private ProductClient productClient;

  @Autowired
  HystrixProductFeignClient hystrixProductFeignClient;

  @Autowired
  private DozerBeanMapper beanMapper;

  public List<ProductTO> getAllProducts() {
    return productClient.getAllProducts().stream()
      .map(product -> beanMapper.map(product, ProductTO.class))
      .map(this::applyDiscountRate)
      .collect(Collectors.toList());
  }

  public List<ProductTO> getProductsWithFeign() {
    return hystrixProductFeignClient.getAllProducts().stream()
            .map(product -> beanMapper.map(product, ProductTO.class))
            .map(this::applyDiscountRate)
            .collect(Collectors.toList());
  }

  public ProductTO getProductByCode(String code) {
    return Optional.ofNullable(productClient.getProductByCode(code))
      .map(product -> beanMapper.map(product, ProductTO.class))
      .map(this::applyDiscountRate)
      .orElse(null);
  }

  public ProductTO getProductByCodeWithFeign(String code) {
    return Optional.ofNullable(hystrixProductFeignClient.getProductByCode(code))
            .map(product -> beanMapper.map(product, ProductTO.class))
            .map(this::applyDiscountRate)
            .orElse(null);
  }

  private ProductTO applyDiscountRate(ProductTO product) {
    product.setPrice(product.getPrice() * (1.0 - properties.getDiscount()));
    return product;
  }
}
