package com.sabbar.orderservice.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderTO {

  private Long id;
  private CustomerData customer;
  private ProductData product;
  private AddressData shippingAddress;

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class CustomerData {
    private String code;
    private String name;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class ProductData {
    private String code;
    private String name;
  }

  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class AddressData {
    private String street;
    private String city;
    private String zip;
    private String state;
  }
}
