package com.sabbar.orderservice.service;

import com.sabbar.orderservice.config.ConfigProperties;
import com.sabbar.orderservice.domain.Address;
import com.sabbar.orderservice.domain.Customer;
import com.sabbar.orderservice.domain.Order;
import com.sabbar.orderservice.domain.Product;
import com.sabbar.orderservice.repository.OrderRepository;
import com.sabbar.orderservice.service.customers.CustomerTO;
import com.sabbar.orderservice.service.customers.ReadCustomerService;
import com.sabbar.orderservice.service.products.ProductTO;
import com.sabbar.orderservice.service.products.ReadProductService;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderService {

  @Autowired
  private ConfigProperties properties;

  @Autowired
  private ReadCustomerService customerService;

  @Autowired
  private ReadProductService productService;

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private DozerBeanMapper beanMapper;

  public OrderTO createOrder(OrderCreationTO orderTO) {

    CustomerTO customerTO = Optional.ofNullable(customerService.getCustomerByCode(orderTO.getCustomerCode()))
      .orElseThrow(() -> new OrderCreationException("Customer " + orderTO.getCustomerCode() + " not found"));

    ProductTO productTO = Optional.ofNullable(productService.getProductByCode(orderTO.getProductCode()))
            .orElseThrow(() -> new OrderCreationException("Product " + orderTO.getProductCode() + " not found"));

    Order order = Order.builder()
      .customer(Customer.builder()
        .code(customerTO.getCode())
        .name(customerTO.getName())
        .build())
      .product(Product.builder()
        .code(productTO.getCode())
        .name(productTO.getName())
        .build())
      .shippingAddress(Address.builder()
        .city(customerTO.getShippingAddress().getCity())
        .state(customerTO.getShippingAddress().getState())
        .street(customerTO.getShippingAddress().getStreet())
        .zip(customerTO.getShippingAddress().getZip())
        .build())
      .build();

    order = orderRepository.save(order);

    return beanMapper.map(order, OrderTO.class);
  }

  public List<OrderTO> getAllOrders() {
    return orderRepository.findAll().stream()
      .map(order -> beanMapper.map(order, OrderTO.class))
      .collect(Collectors.toList());

  }

  public List<OrderTO> getOrderByCustomerCode(String code) {
    return orderRepository.findByCustomerCode(code).stream()
      .map(order -> beanMapper.map(order, OrderTO.class))
      .collect(Collectors.toList());
  }

}
