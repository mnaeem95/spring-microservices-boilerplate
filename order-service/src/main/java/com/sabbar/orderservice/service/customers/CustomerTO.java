package com.sabbar.orderservice.service.customers;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomerTO {

  private String code;
  private String name;
  private AddressTO shippingAddress;
}
