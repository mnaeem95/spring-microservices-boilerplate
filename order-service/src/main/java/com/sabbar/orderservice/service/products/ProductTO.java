package com.sabbar.orderservice.service.products;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductTO {
  private String code;
  private String name;
  private Double price;
  private String description;
}
