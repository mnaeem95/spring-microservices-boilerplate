package com.sabbar.orderservice.client.product;

import java.util.List;

public interface ProductClient {

  public List<RemoteProductTO> getAllProducts();

  public RemoteProductTO getProductByCode(String code);

}
