package com.sabbar.orderservice.client.customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class RemoteCustomerTO {

  private Long id;
  private String code;
  private String name;
  private RemoteAddressTO shippingAddress;
  private RemoteAddressTO billingAddress;

}
