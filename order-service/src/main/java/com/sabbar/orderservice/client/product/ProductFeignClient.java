package com.sabbar.orderservice.client.product;

import org.springframework.cloud.loadbalancer.annotation.LoadBalancerClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "product-service")
@LoadBalancerClient(name = "product-service")
public interface ProductFeignClient {
    @RequestMapping(
            method= RequestMethod.GET,
            value="/products/{code}",
            consumes="application/json")
    RemoteProductTO getProductByCode(@PathVariable("code") String code);

    @RequestMapping(
            method= RequestMethod.GET,
            value="/products",
            consumes="application/json")
    List<RemoteProductTO> getAllProducts();
}
