package com.sabbar.orderservice.client.customer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class RemoteAddressTO {
  private String street;
  private String city;
  private String zip;
  private String state;
}
