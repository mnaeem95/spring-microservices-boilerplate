package com.sabbar.orderservice.client.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class RemoteProductTO {
  private Long id;
  private String code;
  private String name;
  private Double price;
  private String description;
}
