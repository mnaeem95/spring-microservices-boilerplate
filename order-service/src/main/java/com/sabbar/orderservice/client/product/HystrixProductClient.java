package com.sabbar.orderservice.client.product;

import com.sabbar.orderservice.config.ConfigProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class HystrixProductClient implements ProductClient {

  @Autowired
  private RestTemplate restTemplate;

  @Autowired
  private ConfigProperties properties;


  @Override
  @HystrixCommand(
    fallbackMethod = "getAllProductsFallback",
    commandProperties = {
      @HystrixProperty(
        name = "execution.isolation.thread.timeoutInMilliseconds",
        value = "500")
    })
  public List<RemoteProductTO> getAllProducts() {
    return Arrays.asList(restTemplate.getForObject(properties.getProductServiceUrl(), RemoteProductTO[].class));
  }

  public List<RemoteProductTO> getAllProductsFallback() {
    return Collections.emptyList();
  }

  @Override
  @HystrixCommand(
    fallbackMethod = "getProductByCodeFallback",
    commandProperties = {
      @HystrixProperty(
        name = "execution.isolation.thread.timeoutInMilliseconds",
        value = "500")
    })
  public RemoteProductTO getProductByCode(String code) {
    return restTemplate.getForObject(properties.getProductServiceUrl() + code, RemoteProductTO.class);
  }

  public RemoteProductTO getProductByCodeFallback(String code) {
    return null;
  }
}
