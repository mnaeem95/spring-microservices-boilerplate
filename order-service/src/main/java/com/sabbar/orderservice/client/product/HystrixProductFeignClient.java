package com.sabbar.orderservice.client.product;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class HystrixProductFeignClient implements ProductFeignClient {
    @Autowired
    ProductFeignClient productFeignClient;

    @Override
    @HystrixCommand(
            fallbackMethod = "getAllProductsFallback",
            commandProperties = {
                    @HystrixProperty(
                            name = "execution.isolation.thread.timeoutInMilliseconds",
                            value = "500")
            })
    public List<RemoteProductTO> getAllProducts() {
        return productFeignClient.getAllProducts();
    }

    public List<RemoteProductTO> getAllProductsFallback() {
        return Collections.emptyList();
    }

    @Override
    @HystrixCommand(
            fallbackMethod = "getProductByCodeFallback",
            commandProperties = {
                    @HystrixProperty(
                            name = "execution.isolation.thread.timeoutInMilliseconds",
                            value = "500")
            })
    public RemoteProductTO getProductByCode(String code) {
        return productFeignClient.getProductByCode(code);
    }

    public RemoteProductTO getProductByCodeFallback(String code) {
        return null;
    }
}
