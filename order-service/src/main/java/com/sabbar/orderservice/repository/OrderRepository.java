package com.sabbar.orderservice.repository;

import com.sabbar.orderservice.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {

  @Query("select o from Order o where o.customer.code = :code")
  List<Order> findByCustomerCode(@Param("code") String code);

  //JPQL
  @Query("Select o from Order o where o.customer.code = ?1 AND o.product.code = ?2")
  List<Order> findByCustomerCodeAndProductCode(String customerCode, String productCode);

  //Native
  @Query(
          value = "SELECT * FROM ordering o where o.customer.code = ?1",
          nativeQuery = true
  )
  List<Order> findByCustomerCodeNative(String code);

  //Native Named Param
  @Query(
          value = "Select o from ordering o where o.customer.code = :customerCode AND o.product.code = :productCode",
          nativeQuery = true)
  List<Order> selectStudentWhereFirstNameAndAgeGreaterOrEqualNative(String customerCode, String productCode);

}
