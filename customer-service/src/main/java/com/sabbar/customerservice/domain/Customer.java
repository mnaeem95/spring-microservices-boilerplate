package com.sabbar.customerservice.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "customer")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id", updatable = false, nullable = false)
  private Long id;

  @Column(nullable = false)
  private String code;

  @Column(nullable = false)
  private String name;

  @Embedded
  @AttributeOverrides(value = {
    @AttributeOverride(name = "street", column = @Column(name = "shipping_street", nullable = false)),
    @AttributeOverride(name = "city", column = @Column(name = "shipping_city", nullable = false)),
    @AttributeOverride(name = "zip", column = @Column(name = "shipping_zip", nullable = false, length = 6)),
    @AttributeOverride(name = "state", column = @Column(name = "shipping_state", nullable = false))
  })
  private Address shippingAddress;

  @Embedded
  @AttributeOverrides(value = {
    @AttributeOverride(name = "street", column = @Column(name = "billing_street", nullable = false)),
    @AttributeOverride(name = "city", column = @Column(name = "billing_city", nullable = false)),
    @AttributeOverride(name = "zip", column = @Column(name = "billing_zip", nullable = false, length = 6)),
    @AttributeOverride(name = "state", column = @Column(name = "billing_state", nullable = false))
  })
  private Address billingAddress;
}
