package com.sabbar.customerservice.service;

import com.sabbar.customerservice.domain.Customer;
import com.sabbar.customerservice.repository.CustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  public Customer saveCustomer(Customer customer) {
    log.info("Inside saveCustomer of CustomerService");
    return customerRepository.save(customer);
  }

  public void deleteCustomer(Long id) {
    log.info("Inside deleteCustomer of CustomerService");
    customerRepository.deleteById(id);
  }

  public Customer findById(Long id) {
    log.info("Inside findById of CustomerService");
    return Optional.of(customerRepository.findById(id).orElse(null))
      .get();
  }

  public Customer findByCode(String code) {
    return Optional.ofNullable(customerRepository.findByCode(code))
            .orElse(null);
  }

  public List<Customer> findAll() {
    log.info("Inside findAll of CustomerService");
    return customerRepository.findAll().stream()
      .collect(Collectors.toList());
  }

}
