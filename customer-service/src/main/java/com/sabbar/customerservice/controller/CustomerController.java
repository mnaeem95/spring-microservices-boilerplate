package com.sabbar.customerservice.controller;

import com.sabbar.customerservice.domain.Customer;
import com.sabbar.customerservice.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/customers")
@Slf4j
public class CustomerController {

  @Autowired
  private CustomerService customerService;

  @GetMapping(path = "/{code}")
  public ResponseEntity<Customer> getCustomer(@PathVariable String code) {
    return Optional.ofNullable(customerService.findByCode(code))
            .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @GetMapping()
  public ResponseEntity<List<Customer>> getCustomers() {
    log.info("Inside getCustomers of CustomerController");
    return Optional.ofNullable(customerService.findAll())
      .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  @PostMapping()
  public ResponseEntity<Customer> saveOrUpdate(@RequestBody Customer customer) {
    log.info("Inside saveOrUpdate of CustomerController");
    return Optional.ofNullable(customerService.saveCustomer(customer))
      .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
      .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

}
