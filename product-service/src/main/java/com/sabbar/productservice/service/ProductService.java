package com.sabbar.productservice.service;

import com.sabbar.productservice.domain.Product;
import com.sabbar.productservice.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class ProductService {

  @Autowired
  private ProductRepository productRepository;

  public Product saveProduct(Product product) {
    log.info("Inside saveProduct of ProductService");
    return productRepository.save(product);
  }

  public void deleteProduct(long id) {
    log.info("Inside deleteProduct of ProductService");
    productRepository.deleteById(id);
  }

  public Product findById(long id) {
    log.info("Inside findById of ProductService");
    return Optional.of(productRepository.findById(id).orElse(null))
      .get();
  }

  public Product findByCode(String code) {
    log.info("Inside findByCode of ProductService");
    return Optional.ofNullable(productRepository.findByCode(code))
            .orElse(null);
  }

  public List<Product> findAll() {
    log.info("Inside findAll of ProductService");
    return productRepository.findAll().stream()
      .collect(Collectors.toList());
  }

}
