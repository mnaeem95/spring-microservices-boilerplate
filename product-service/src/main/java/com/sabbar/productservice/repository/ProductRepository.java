package com.sabbar.productservice.repository;

import com.sabbar.productservice.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    public Product findByCode(String code);
    public List<Product> findByName(String name);
    public Product findByNameAndCode(String name, String code);
}
