package com.sabbar.productservice.controller;

import com.sabbar.productservice.domain.Product;
import com.sabbar.productservice.exceptions.ProductNotfoundException;
import com.sabbar.productservice.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/products")
@Slf4j
public class ProductController {

  @Autowired
  private ProductService productService;

  @GetMapping(path = "/{code}")
  public ResponseEntity<Product> getProduct(@PathVariable String code) {
    log.info("Inside getProduct method of ProductController");
    return Optional.ofNullable(productService.findByCode(code))
      .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
      .orElseThrow(ProductNotfoundException::new);
  }

  @GetMapping()
  public ResponseEntity<List<Product>> getProducts() {
    log.info("Inside getProducts method of ProductController");
    return Optional.ofNullable(productService.findAll())
      .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
      .orElseThrow(ProductNotfoundException::new);
  }

  @PostMapping()
  public ResponseEntity<Product> saveOrUpdate(@Validated  @RequestBody Product product) {
    log.info("Inside saveOrUpdate method of ProductController");
    return Optional.ofNullable(productService.saveProduct(product))
      .map(c -> new ResponseEntity<>(c, HttpStatus.OK))
      .orElseThrow(ProductNotfoundException::new);
  }

}
